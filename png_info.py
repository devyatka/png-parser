import chunks
import parse_png
import re
import binascii
from zlib import decompress

# pixheart2 (colortype 3)
# pi (colortype 6)
# pok (3)
# picachu


class PngInfo:
    def __init__(self):
        self._IHDR = None
        self._PLTE = None
        self._IDATs = []
        self._cHRM = None
        self._gAMA = None
        self._iCCP = None
        self._sRGB = None
        self._bKGD = None
        self._tRNS = None
        self._pHYs = None
        self._tIME = None
        self._iTXt = None
        self._tEXt = None
        self._zTXt = None
        self._sBIT = None

    def get_pixel_width(self):
        if self.IHDR.colour_type == 0 or self.IHDR.colour_type == 3:
            return 1
        elif self.IHDR.colour_type == 4:
            return 2
        elif self.IHDR.colour_type == 2:
            return 3
        else:
            return 4

    def get_bitmap(self):
        deflate = b''.join([idat.data for idat in self.IDATs])
        deflate = binascii.unhexlify(deflate)
        raw = decompress(deflate)
        pixel_raw = []
        pix_width = self.get_pixel_width()
        step = self.IHDR.width * pix_width + 1
        for i in range(0, len(raw), step):
            pixel_raw.append(raw[i + 1:i + self.IHDR.width * pix_width + 1])
        return b''.join(pixel_raw)

    def get_rgba(self, x, y):
        pxl = self.get_pixel(x, y)
        r, g, b, alpha = 255, 255, 255, 255
        if self.IHDR.colour_type == 0:
            gray = pxl[0]
            r, g, b, alpha = gray, gray, gray, 255
        elif self.IHDR.colour_type == 2:
            r, g, b, alpha = pxl[0], pxl[1], pxl[2], 255
        elif self.IHDR.colour_type == 3:
            rgb = binascii.unhexlify(self.PLTE.data)[pxl[0] * 3:pxl[0] * 3 + 3]
            if len(rgb) == 3:
                r, g, b, alpha = rgb[0], rgb[1], rgb[2], 255
            else:
                r, g, b, alpha = 0, 0, 0, 255
            print('colortype 3\t' + str(r) + ' ' + str(g) + ' '
                  + str(b) + ' ' + str(alpha))
        elif self.IHDR.colour_type == 4:
            gray = pxl[0]
            r, g, b, alpha = gray, gray, gray, pxl[1]
        elif self.IHDR.colour_type == 6:
            print('color 6\t' + str(pxl))
            r, g, b, alpha = pxl[0], pxl[1], pxl[2], pxl[3]
        return r, g, b, alpha

    def get_pixel(self, x, y):
        bitmap = self.get_bitmap()

        pixel = [c for c in bitmap[
                            (self.IHDR.width * y + x) * self.get_pixel_width():
                            (self.IHDR.width * y + x + 1) *
                            self.get_pixel_width()]]
        return pixel
        # if len(pixel) == 4:
        #     return pixel
        # elif len(pixel) == 3:
        #     return pixel.append(255)
        # else:
        #     return [0, 0, 0, 255]

    @property
    def IHDR(self):
        return self._IHDR

    @IHDR.setter
    def IHDR(self, IHDR):
        self._IHDR = IHDR

    @property
    def PLTE(self):
        return self._PLTE

    @PLTE.setter
    def PLTE(self, PLTE):
        self._PLTE = PLTE

    @property
    def IDATs(self):
        return self._IDATs

    @IDATs.setter
    def IDATs(self, IDAT):
        self._IDATs.append(IDAT)

    @property
    def cHRM(self):
        return self._cHRM

    @cHRM.setter
    def cHRM(self, cHRM):
        self._cHRM = cHRM

    @property
    def gAMA(self):
        return self._gAMA

    @gAMA.setter
    def gAMA(self, gAMA):
        self._gAMA = gAMA

    @property
    def iCCP(self):
        return self._iCCP

    @iCCP.setter
    def iCCP(self, iCCP):
        self._iCCP = iCCP

    @property
    def sRGB(self):
        return self._sRGB

    @sRGB.setter
    def sRGB(self, sRGB):
        self._sRGB = sRGB

    @property
    def bKGD(self):
        return self._bKGD

    @bKGD.setter
    def bKGD(self, bKGD):
        self._bKGD = bKGD

    @property
    def tRNS(self):
        return self._tRNS

    @tRNS.setter
    def tRNS(self, tRNS):
        self._tRNS = tRNS

    @property
    def pHYs(self):
        return self._pHYs

    @pHYs.setter
    def pHYs(self, pHYs):
        self._pHYs = pHYs

    @property
    def tIME(self):
        return self._tIME

    @tIME.setter
    def tIME(self, tIME):
        self._tIME = tIME

    @property
    def iTXt(self):
        return self._iTXt

    @iTXt.setter
    def iTXt(self, iTXt):
        self._iTXt = iTXt

    @property
    def tEXt(self):
        return self._tEXt

    @tEXt.setter
    def tEXt(self, tEXt):
        self._tEXt = tEXt

    @property
    def zTXt(self):
        return self._zTXt

    @zTXt.setter
    def zTXt(self, zTXt):
        self._zTXt = zTXt

    @property
    def sBIT(self):
        return self._sBIT

    @sBIT.setter
    def sBIT(self, sBIT):
        self._sBIT = sBIT

    def print_all_info(self):
        print(self.get_all_info_str())

    def get_all_info_str(self):
        text = []
        for item, value in self.__dict__.items():
            if value is not None:
                if item == '_IDATs':
                    continue
                    # for idat in value:
                    #     text.append(get_chunk_info_str(idat))
                else:
                    text.append(get_chunk_info_str(value))
        return '\n'.join(text)


# netu sPLT, hIST
def make_image_info(image):
    data = image.read()
    text = binascii.hexlify(data)
    if text[:16] != b'89504e470d0a1a0a':
        raise ValueError('Not a PNG!')

    all_chunks = ['IHDR', 'PLTE', 'IEND', 'cHRM', 'gAMA', 'iCCP',
                  'sBIT', 'sRGB', 'bKGD', 'hIST', 'tRNS',
                  'pHYs', 'sPLT', 'tIME', 'iTXt', 'tEXt', 'zTXt']
    all_chunk_positions = [re.search(
        (''.join([hex(ord(char))[2:] for char in all_chunks[index]])),
        str(text)) for index in range(len(all_chunks))]
    positions = []
    for pos in all_chunk_positions:
        if pos is not None:
            positions.append(pos.start())
        else:
            positions.append(0)
    all_IDAT_chunk_pos = [pos.start() for pos in
                          re.finditer('49444154', str(text))]
    for pos in all_IDAT_chunk_pos:
        all_chunks.append('IDAT')
        positions.append(pos)

    image_chunks = sorted(
        [chunk for chunk in zip(all_chunks, positions) if chunk[1] != 0],
        key=get_key)

    print(image_chunks)  # убрать потом

    info = PngInfo()

    for i in range(len(image_chunks) - 1):
        chunk_data = text[image_chunks[i][1] - 10:image_chunks[i + 1][1] - 10]
        current_chunk = image_chunks[i][0]
        if current_chunk == 'IHDR':
            info.IHDR = chunks.parse_IHDR(chunk_data)
        elif current_chunk == 'IDAT':
            info.IDATs.append(chunks.parse_IDAT(chunk_data))
        elif current_chunk == 'tRNS' or current_chunk == 'bKGD'\
                or current_chunk == 'sBIT':
            ch = eval('chunks.parse_{}'.format(
                current_chunk))(chunk_data, info.IHDR.colour_type)
            setattr(info, current_chunk, ch)
        else:
            ch = eval('chunks.parse_{}'.format(current_chunk))(chunk_data)
            setattr(info, current_chunk, ch)

    return info


def get_key(item):
    return item[1]


# def print_chunk_info(chunk):
#     print(chunk.__class__.__name__ + ':')
#     for item, value in chunk.__dict__.items():
#         if item == 'length':
#             continue
#         print('\t' + item + ' : ' + str(value))


def print_chunk_info(chunk):
    print(get_chunk_info_str(chunk))


def get_chunk_info_str(chunk):
    text = []
    text.append(chunk.__class__.__name__ + ':')
    for item, value in chunk.__dict__.items():
        if item == 'length' or item == 'data' or item == 'crc':
            continue
        text.append('\t' + item + ' : ' + str(value))
    return '\n'.join(text)
