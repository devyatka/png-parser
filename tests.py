import unittest
import chunks


class TestParsingChunks(unittest.TestCase):
    def test_parse_IHDR(self):
        data = b'0000000d49484452000002530000022408030000000db85ea3'
        result_chunk = chunks.parse_IHDR(data)
        self.assertIsInstance(result_chunk, chunks.IHDR_chunk)
        self.assertEqual(result_chunk.length, 13)
        self.assertEqual(result_chunk.width, 595)
        self.assertEqual(result_chunk.height, 548)
        self.assertEqual(result_chunk.bit_depth, 8)
        self.assertEqual(result_chunk.colour_type, 3)
        self.assertEqual(result_chunk.compr_method, 0)
        self.assertEqual(result_chunk.filter_method, 0)
        self.assertEqual(result_chunk.interlace_method, 0)
        self.assertEqual(result_chunk.crc, b'0db85ea3')

        bad_data = b'123123123123'
        with self.assertRaises(ValueError):
            chunks.parse_IHDR(bad_data)

        bad_compression = b'0000000d49484452000002530000022408030500000db85ea3'
        with self.assertRaises(ValueError):
            chunks.parse_IHDR(bad_compression)

        bad_filter = b'0000000d49484452000002530000022408030004000db85ea3'
        with self.assertRaises(ValueError):
            chunks.parse_IHDR(bad_filter)

        bad_interlace = b'0000000d49484452000002530000022408030000050db85ea3'
        with self.assertRaises(ValueError):
            chunks.parse_IHDR(bad_interlace)

    def test_parse_IDAT(self):
        data = b'0000000f4944415412312312312312312312300000123'
        result_chunk = chunks.parse_IDAT(data)
        self.assertIsInstance(result_chunk, chunks.IDAT_chunk)
        self.assertEqual(result_chunk.length, 15)
        self.assertEqual(result_chunk.data, b'123123123123123123123')
        self.assertEqual(result_chunk.crc, b'00000123')

        bad_data = b'123123'
        with self.assertRaises(ValueError):
            chunks.parse_IDAT(bad_data)

    def test_parse_gAMA(self):
        data = b'0000000467414d410000b18f0bfc6105'
        result_chunk = chunks.parse_gAMA(data)
        self.assertIsInstance(result_chunk, chunks.gAMA_chunk)
        self.assertEqual(result_chunk.length, 4)
        self.assertEqual(result_chunk.gamma, 0)
        self.assertEqual(result_chunk.crc, b'0bfc6105')

        bad_data = b'123123'
        with self.assertRaises(ValueError):
            chunks.parse_gAMA(bad_data)

    def test_parse_PLTE(self):
        data = b'0000000c504c5445ffffff00000014141415220015242a33172'
        result_chunk = chunks.parse_PLTE(data)
        self.assertIsInstance(result_chunk, chunks.PLTE_chunk)
        self.assertEqual(result_chunk.length, 12)
        self.assertEqual(result_chunk.rgb_entries, 4)
        self.assertEqual(result_chunk.crc, b'42a33172')

        bad_data = b'123'
        with self.assertRaises(ValueError):
            chunks.parse_PLTE(bad_data)

        bad_data = b'0000000dc504c544536f6a3e9043'
        with self.assertRaises(ValueError):
            chunks.parse_PLTE(bad_data)

    def test_parse_sRGB(self):
        data = b'000000017352474200aece1ce9'
        result_chunk = chunks.parse_sRGB(data)
        self.assertIsInstance(result_chunk, chunks.sRGB_chunk)
        self.assertEqual(result_chunk.length, 1)
        self.assertEqual(result_chunk.rendering_intent, 0)
        self.assertEqual(result_chunk.crc, b'aece1ce9')

        bad_data = b'123'
        with self.assertRaises(ValueError):
            chunks.parse_sRGB(bad_data)

        bad_data = b'110000000000000000000000'
        with self.assertRaises(ValueError):
            chunks.parse_sRGB(bad_data)

    def test_parse_iTXt(self):
        data = b'0000001d69545874436f6d6d656e7400ff01aabbcc0074657374004372656174656420776974682047494d50642e6507'
        result_chunk = chunks.parse_iTXt(data)
        self.assertIsInstance(result_chunk, chunks.iTXt_chunk)
        self.assertEqual(result_chunk.length, 29)
        self.assertEqual(result_chunk.keyword, 'Comment')
        self.assertEqual(result_chunk.compression_flag, 255)
        self.assertEqual(result_chunk.compression_method, 1)
        self.assertEqual(result_chunk.language_tag, b'aabbcc')
        self.assertEqual(result_chunk.translated_keyword, 'test')
        self.assertEqual(result_chunk.text, 'Created with GIMP')
        self.assertEqual(result_chunk.crc, b'642e6507')

        bad_data = b'123'
        with self.assertRaises(ValueError):
            chunks.parse_iTXt(bad_data)

    def test_parse_cHRM(self):
        data = b'000000206348524d00007a26000080840000fa00000080e8000075300000ea6000003a98000017709cba513c'
        result_chunk = chunks.parse_cHRM(data)
        self.assertIsInstance(result_chunk, chunks.cHRM_chunk)
        self.assertEqual(result_chunk.length, 32)
        self.assertEqual(result_chunk.white_pointX, 31270)
        self.assertEqual(result_chunk.white_pointY, 32900)
        self.assertEqual(result_chunk.redX, 64000)
        self.assertEqual(result_chunk.redY, 33000)
        self.assertEqual(result_chunk.greenX, 30000)
        self.assertEqual(result_chunk.greenY, 60000)
        self.assertEqual(result_chunk.blueX, 15000)
        self.assertEqual(result_chunk.blueY, 6000)
        self.assertEqual(result_chunk.crc, b'9cba513c')

        bad_data = b'123123123'
        with self.assertRaises(ValueError):
            chunks.parse_cHRM(bad_data)

    def test_parse_tIME(self):
        data = b'0000000774494d4507e10219133201c20a503c'
        result_chunk = chunks.parse_tIME(data)
        self.assertIsInstance(result_chunk, chunks.tIME_chunk)
        self.assertEqual(result_chunk.length, 7)
        self.assertEqual(result_chunk.year, 2017)
        self.assertEqual(result_chunk.month, 2)
        self.assertEqual(result_chunk.day, 25)
        self.assertEqual(result_chunk.hour, 19)
        self.assertEqual(result_chunk.minute, 50)
        self.assertEqual(result_chunk.second, 1)
        self.assertEqual(result_chunk.crc, b'c20a503c')

        bad_data = b'123000000001231363763365376277273'
        with self.assertRaises(ValueError):
            chunks.parse_tIME(bad_data)

        bad_data = b'123'
        with self.assertRaises(ValueError):
            chunks.parse_tIME(bad_data)

    def test_parse_tRNS(self):
        data_color0 = b'0000000274524e53138812345678'
        result_chunk = chunks.parse_tRNS(data_color0, 0)
        self.assertIsInstance(result_chunk, chunks.tRNS_chunk)
        self.assertEqual(result_chunk.length, 2)
        self.assertEqual(result_chunk.grey_sample_value, 5000)
        self.assertEqual(result_chunk.crc, b'12345678')

        data_color2 = b'0000000674524e53aaaabbbbcccc88776655'
        result_chunk = chunks.parse_tRNS(data_color2, 2)
        self.assertIsInstance(result_chunk, chunks.tRNS_chunk)
        self.assertEqual(result_chunk.length, 6)
        self.assertEqual(result_chunk.red_sample_value, 43690)
        self.assertEqual(result_chunk.green_sample_value, 52428)
        self.assertEqual(result_chunk.blue_sample_value, 48059)
        self.assertEqual(result_chunk.crc, b'88776655')

        data_color3 = b'0000000374524e53aabbcc12344321'
        result_chunk = chunks.parse_tRNS(data_color3, 3)
        self.assertIsInstance(result_chunk, chunks.tRNS_chunk)
        self.assertEqual(result_chunk.length, 3)
        self.assertEqual(result_chunk.alpha_for_palette0, 170)
        self.assertEqual(result_chunk.alpha_for_palette1, 187)
        self.assertEqual(result_chunk.etc, b'cc')
        self.assertEqual(result_chunk.crc, b'12344321')

        bad_color = 1
        with self.assertRaises(ValueError):
            chunks.parse_tRNS(data_color0, bad_color)

        short_data = b'123'
        bad_length_data = b'7777263374524e53aabbcc12344321'
        with self.assertRaises(ValueError):
            chunks.parse_tRNS(short_data, 0)
        with self.assertRaises(ValueError):
            chunks.parse_tRNS(bad_length_data, 2)

    def test_parse_iCCP(self):
        data = b'0000000f69434350746573740001636f6d7072657373656487654321'
        result_chunk = chunks.parse_iCCP(data)
        self.assertIsInstance(result_chunk, chunks.iCCP_chunk)
        self.assertEqual(result_chunk.length, 15)
        self.assertEqual(result_chunk.profile_name, 'test')
        self.assertEqual(result_chunk.compression_method, 1)
        self.assertEqual(result_chunk.compressed_profile, 'compressed')
        self.assertEqual(result_chunk.crc, b'87654321')

        bad_data = b'12345'
        with self.assertRaises(ValueError):
            chunks.parse_iCCP(bad_data)

    def test_parse_pHYs(self):
        data = b'000000097048597300000b1300000b130188899988'
        result_chunk = chunks.parse_pHYs(data)
        self.assertIsInstance(result_chunk, chunks.pHYs_chunk)
        self.assertEqual(result_chunk.length, 9)
        self.assertEqual(result_chunk.pixels_per_unitX, 2835)
        self.assertEqual(result_chunk.pixels_for_unitY, 2835)
        self.assertEqual(result_chunk.unit_specifier, 1)
        self.assertEqual(result_chunk.crc, b'88899988')

        bad_data_length = b'778899777048597300000b1300000b130188899988'
        with self.assertRaises(ValueError):
            chunks.parse_pHYs(bad_data_length)
        bad_data = b'56789'
        with self.assertRaises(ValueError):
            chunks.parse_pHYs(bad_data)

    def test_parse_tEXt(self):
        data = b'00000089744558746b6579776f72640068747470733a2f2f7777772e676f6f676c652e636f6d207465737477778888'
        result_chunk = chunks.parse_tEXt(data)
        self.assertIsInstance(result_chunk, chunks.tEXt_chunk)
        self.assertEqual(result_chunk.length, 137)
        self.assertEqual(result_chunk.keyword, 'keyword')
        self.assertEqual(result_chunk.text, 'https://www.google.com test')
        self.assertEqual(result_chunk.crc, b'77778888')

        bad_data = b'556677'
        with self.assertRaises(ValueError):
            chunks.parse_tEXt(bad_data)

        no_separator_data = b'00000089744558746b6579998877665544332211223344556677'
        with self.assertRaises(ValueError):
            chunks.parse_tEXt(no_separator_data)

    def test_parse_zTXt(self):
        data = b'000000567a545874736c6f6e6f706f74616d0000636f6d70726573736564207465787433344455'
        result_chunk = chunks.parse_zTXt(data)
        self.assertIsInstance(result_chunk, chunks.zTXt_chunk)
        self.assertEqual(result_chunk.length, 86)
        self.assertEqual(result_chunk.keyword, 'slonopotam')
        self.assertEqual(result_chunk.compression_method, 0)
        self.assertEqual(result_chunk.compessed_text, 'compressed text')
        self.assertEqual(result_chunk.crc, b'33344455')

        bad_data = b'34567883'
        with self.assertRaises(ValueError):
            chunks.parse_zTXt(bad_data)
        no_separator_data = b'000000567a545874736c6f6e6f706f74616'
        with self.assertRaises(ValueError):
            chunks.parse_zTXt(no_separator_data)

    def test_parse_bKGD(self):
        data_color0 = b'00000002624b4744aaaa88885555'
        result_chunk = chunks.parse_bKGD(data_color0, 0)
        self.assertIsInstance(result_chunk, chunks.bKGD_chunk)
        self.assertEqual(result_chunk.length, 2)
        self.assertEqual(result_chunk.grayscale, 43690)
        self.assertEqual(result_chunk.crc, b'88885555')

        data_color2 = b'00000006624b4744aaaabbbbcccc12341234'
        result_chunk = chunks.parse_bKGD(data_color2, 2)
        self.assertIsInstance(result_chunk, chunks.bKGD_chunk)
        self.assertEqual(result_chunk.length, 6)
        self.assertEqual(result_chunk.red, 43690)
        self.assertEqual(result_chunk.green, 48059)
        self.assertEqual(result_chunk.blue, 52428)
        self.assertEqual(result_chunk.crc, b'12341234')

        data_color3 = b'00000001624b4744ff33336666'
        result_chunk = chunks.parse_bKGD(data_color3, 3)
        self.assertIsInstance(result_chunk, chunks.bKGD_chunk)
        self.assertEqual(result_chunk.length, 1)
        self.assertEqual(result_chunk.palette_index, 255)
        self.assertEqual(result_chunk.crc, b'33336666')

        with self.assertRaises(ValueError):
            chunks.parse_bKGD(data_color0, 2)
        with self.assertRaises(ValueError):
            chunks.parse_bKGD(data_color3, 0)
        with self.assertRaises(ValueError):
            chunks.parse_bKGD(data_color2, 9)

    def test_parse_sPLT(self):
        data_depth8 = b'000000a973504c5470616c6c65746b6b610008aabbccddaaaa90908080'
        result_chunk = chunks.parse_sPLT(data_depth8)
        self.assertIsInstance(result_chunk, chunks.sPLT_chunk)
        self.assertEqual(result_chunk.length, 169)
        self.assertEqual(result_chunk.palette_name, 'palletkka')
        self.assertEqual(result_chunk.sample_depth, 8)
        self.assertEqual(result_chunk.red, 170)
        self.assertEqual(result_chunk.green, 187)
        self.assertEqual(result_chunk.blue, 204)
        self.assertEqual(result_chunk.alpha, 221)
        self.assertEqual(result_chunk.frequensy, 43690)
        self.assertEqual(result_chunk.crc, b'90908080')

        data_depth16 = b'000000ef73504c54636f64656e616d650010aaaabbbbccccddddeeee55667755'
        result_chunk = chunks.parse_sPLT(data_depth16)
        self.assertIsInstance(result_chunk, chunks.sPLT_chunk)
        self.assertEqual(result_chunk.length, 239)
        self.assertEqual(result_chunk.palette_name, 'codename')
        self.assertEqual(result_chunk.sample_depth, 16)
        self.assertEqual(result_chunk.red, 43690)
        self.assertEqual(result_chunk.green, 48059)
        self.assertEqual(result_chunk.blue, 52428)
        self.assertEqual(result_chunk.alpha, 56797)
        self.assertEqual(result_chunk.frequensy, 61166)
        self.assertEqual(result_chunk.crc, b'55667755')

    def test_parse_IEND(self):
        data = b'49454e44ae426082'
        result_chunk = chunks.parse_IEND(data)
        self.assertIsInstance(result_chunk, chunks.IEND_chunk)
        self.assertEqual(result_chunk.crc, b'ae426082')

        bad_data = [b'12', b'12989f9e09229ad89f3e9a0c', b'0000000000000000000000', b'49454e44ae42608']
        for data in bad_data:
            with self.assertRaises(ValueError):
                chunks.parse_IEND(data)
