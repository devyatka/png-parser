import os
import re
import binascii
from string import ascii_letters
import codecs
import argparse
import png_info


# PLTE.length must delitsa na 3
# IDAT.length must be 13
# PNGMAGIC
# IEND 49454e44ae426082
# iTXt ИСПРАВИТЬ СРОЧНА
# кстати тесты пора катать
# tRNS not for colour types 4 and 6


# good obrabotano (nu ne good, but snosno):    (except crc)
# tIME
# tEXt
# IDAT (nu tut prosto hz poka)


# steganochka: If the image bit depth is less than 16, the least significant bits are used and the others are 0


def init_arguments():
    parser = argparse.ArgumentParser(prog='PNG-parser',
                                     usage='parser.py -f filename')
    parser.add_argument('-f', '--file', help='PNG-image', default=None)
    return parser


def main():
    # parser = init_arguments()
    # if parser.parse_args().file is None:
    #     print('usage:', parser.usage)
    #     return
    #
    # path = parser.parse_args().file
    # filename, file_extension = os.path.splitext(path)
    # short_name = filename.split('/')
    #
    # if file_extension != '.png':
    #     raise NameError('This parser is for PNG-files!')

    # short_name = 'stab'
    #
    # file = open('result.txt', 'w', encoding='utf-8')
    # file.write('\nAnalysis of PNG-file: {}\n'.format(short_name))
    # file.close()

    # with open(path, 'rb') as image:
    #     make_image_info(image)

    # png_parser('help.png')   # IHDR, tEXt, IDAT, IEND
    # png_parser('cat.png')  # IHDR, sRGB, IDAT, IEND
    # png_parser('jump.png')
    # png_parser('bbb.png')
    # png_parser('ban.png')   # CT 6
    # png_parser('gits.png')  # IHDR, zTXt, IDAT, IEND
    # png_parser('kot.png')   # шо тут происходит??
    png_parser('rdg.png')  # IHDR, sRGB, PLTE, pHYs, tIME, iTXt, IDAT, IEND
    # png_parser('ps.png')  # IHDR, pHYs, iCCP, cHRM, IDAT, IEND
    # png_parser('wiki.png')   # IHDR, gAMA, cHRM, bKGD, tIME, IDAT, tEXt, IEND
    # png_parser('lll.png')  # IHDR, gAMA, sRGB, PLTE, IDAT, IEND
    # png_parser('transp.png')  # IHDR, bKGD, pHYs, tIME, iTXt, IDAT, IEND

    # png_parser('fst.png')  # IHDR, sRGB, gAMA, pHYs, tEXt, PLTE, tRNS, IDAT, IEND
    # png_parser('cot.png')  # IHDR, tEXt, iTXt, PLTE, tRNS, IDAT, IEND                        herota s iTXt
    # png_parser('tst.png')  # dlya tIME


def png_parser(path):
    file_extension = path.split('.')[1]
    if file_extension != 'png':
        raise NameError('This parser is for PNG-files!')

    with open(path, 'rb') as image:
        png_info.make_image_info(image)


class IHDR_chunk:
    def __init__(self, length, width, height, bit_depth, colour_type,
                 compr_method, filter_method, interlace_method,
                 crc):
        self.length = int(length, 16)
        self.width = int(width, 16)
        self.height = int(height, 16)
        self.bit_depth = int(bit_depth, 16)
        self.colour_type = int(colour_type, 16)
        self.compr_method = int(compr_method, 16)
        self.filter_method = int(filter_method, 16)
        self.interlace_method = int(interlace_method, 16)
        self.crc = crc


def parse_IHDR(data):
    if len(data) != 50:
        raise ValueError('IHDR chunk is damaged, image cannot be parsed '
                         'correctly!')
    IHDR = IHDR_chunk(data[:8], data[16:24].decode('utf-8'),
                      data[24:32].decode('utf-8'),
                      data[32:34].decode('utf-8'), data[34:36].decode('utf-8'),
                      data[36:38].decode('utf-8'),
                      data[38:40].decode('utf-8'), data[40:42].decode('utf-8'),
                      data[-8:])
    if IHDR.interlace_method != 0 and IHDR.interlace_method != 1:
        raise ValueError('IHDR chunk is damaged, image cannot be parsed'
                         ' correctly!')
    if IHDR.filter_method != 0 or IHDR.compr_method != 0:
        raise ValueError('IHDR chunk is damaged, image cannot be parsed'
                         ' correctly!')
    return IHDR


class sBIT_chunk:
    def __init__(self, length, sign_gray, sign_red, sign_green, sign_blue,
                 sign_alpha, crc):
        self.length = int(length, 16)
        if sign_gray is not None:
            self.significant_gray_bits = int(sign_gray, 16)
        if sign_alpha is not None:
            self.significant_alpha_bits = int(sign_alpha, 16)
        if sign_red is not None:
            self.significant_red_bits = int(sign_red, 16)
            self.significant_green_bits = int(sign_green, 16)
            self.significant_blue_bits = int(sign_blue, 16)
        self.crc = crc


def parse_sBIT(data, colour_type):
    if colour_type > 6 or colour_type == 5 or len(data) < 13:
        raise ValueError('sBIT chunk cannot be parsed correctly, unknown '
                         'colourtype')
    sign_gray, sign_red, sign_green, sign_blue, sign_alpha = None, None, \
                                                             None, None, None
    if colour_type == 0:
        sign_gray = data[16:18]
    if colour_type == 2 or colour_type == 3:
        sign_red = data[16:18]
        sign_blue = data[18:20]
        sign_green = data[20:22]
    if colour_type == 4:
        sign_gray = data[16:18]
        sign_alpha = data[18:20]
    if colour_type == 6:
        sign_red = data[16:18]
        sign_green = data[18:20]
        sign_blue = data[20:22]
        sign_alpha = data[22:24]
    return sBIT_chunk(data[:8], sign_gray, sign_red, sign_green, sign_blue,
                      sign_alpha, data[-8:])


class PLTE_chunk:
    def __init__(self, length, data, crc):
        self.length = int(length, 16)
        self.data = data
        self.rgb_entries = self.length / 3
        self.crc = crc


def parse_PLTE(data):
    print('PLTE DATA')
    print(binascii.unhexlify(data))
    print(binascii.unhexlify(data))
    if int(data[:8], 16) % 3 != 0 or len(data) < 13:
        raise ValueError('PLTE chunk cannot be parsed correctly')
    return PLTE_chunk(data[:8], data[16:-8], data[-8:])


class IDAT_chunk:
    def __init__(self, length, data, crc):
        self.length = int(length, 16)
        self.data = data
        self.crc = crc


def parse_IDAT(data):
    if len(data) < 13:
        raise ValueError('IDAT chunk cannot be parsed correctly')
    return IDAT_chunk(data[:8], data[16:-8], data[-8:])


class iCCP_chunk:
    def __init__(self, length, profile, compr_method, compr_profile, crc):
        self.length = int(length, 16)
        self.profile_name = codecs.decode(profile, 'hex').decode('utf-8')
        self.compression_method = int(compr_method, 16)
        self.compressed_profile = codecs.decode(
            compr_profile, 'hex').decode('utf-8', 'ignore')
        self.crc = crc


def parse_iCCP(data):
    if len(data) < 13:
        raise ValueError('iCCP chunk cannot be parsed correctly')
    separator = re.search('00', str(data[8:]))
    return iCCP_chunk(data[:8], data[16:separator.start() + 6],
                      data[separator.start() + 8:separator.start() + 10],
                      data[separator.start() + 10:-8], data[-8:])


# There may be any number of entries. A PNG decoder determines the number of entries from the length of the chunk
# remaining after the sample depth byte. This shall be divisible by 6 if the sPLT sample depth is 8, or by 10 if
# the sPLT sample depth is 16
# Entries shall appear in decreasing order of frequency.
# Palette names shall contain only printable Latin-1 characters and spaces
# (only character codes 32-126 and 161-255 decimal are allowed).
# The sPLT sample depth shall be 8 or 16.
# The red, green, blue, and alpha samples are either one or two bytes each, depending on the sPLT sample depth,
# regardless of the image bit depth.
# An alpha value of 0 means fully transparent. An alpha value of 255 (when the sPLT sample depth is 8) or 65535
# (when the sPLT sample depth is 16) means fully opaque. The sPLT chunk may appear for any PNG colour type.
# Multiple sPLT chunks are permitted, but each shall have a different palette name.
class sPLT_chunk:
    def __init__(self, length, name, sample_depth, red, green, blue, alpha,
                 frequency, crc):
        self.length = int(length, 16)
        self.palette_name = codecs.decode(name, 'hex').decode('latin-1')
        self.sample_depth = sample_depth
        self.red = int(red, 16)
        self.green = int(green, 16)
        self.blue = int(blue, 16)
        self.alpha = int(alpha, 16)
        self.frequensy = int(frequency, 16)
        self.crc = crc


def parse_sPLT(data):
    separator = str(data).find('00', 8)
    sample_depth = int(data[separator:separator + 2], 16)
    if sample_depth == 8:
        red = data[separator + 2:separator + 4]
        green = data[separator + 4:separator + 6]
        blue = data[separator + 6:separator + 8]
        alpha = data[separator + 8:separator + 10]
        frequency = data[separator + 10:separator + 14]
    else:
        if sample_depth == 16:
            red = data[separator + 2:separator + 6]
            green = data[separator + 6:separator + 10]
            blue = data[separator + 10:separator + 14]
            alpha = data[separator + 14:separator + 18]
            frequency = data[separator + 18:separator + 22]
        else:
            print('sPLT chunk can\'t be parsed')
            return
    return sPLT_chunk(data[:8], data[16:separator - 2], sample_depth, red,
                      green, blue, alpha, frequency, data[-8:])


class zTXt_chunk:
    def __init__(self, length, keyword, compr_method, compr_text, crc):
        self.length = int(length, 16)
        self.keyword = codecs.decode(keyword, 'hex').decode('utf-8', 'ignore')
        self.compression_method = int(compr_method, 16)
        self.compessed_text = codecs.decode(compr_text, 'hex').decode(
            'latin-1', 'ignore')
        self.crc = crc


def parse_zTXt(data):
    if len(data) < 13:
        raise ValueError('zTXt chunk cannot be parsed correctly')
    separator = re.search('00', str(data[8:]))
    if separator is None:
        raise ValueError('zTXt chunk cannot be parsed correctly')
    return zTXt_chunk(data[:8], data[16:separator.start() + 6], data[separator.start() + 8:separator.start() + 10],
                      data[separator.start() + 10:-8], data[-8:])


class tRNS_chunk:
    def __init__(self, length, grey, red, blue, green, alpha0, alpha1, etc, crc):
        self.length = length
        if grey is not None:
            self.grey_sample_value = int(grey, 16)
        if red is not None:
            self.red_sample_value = int(red, 16)
            self.blue_sample_value = int(blue, 16)
            self.green_sample_value = int(green, 16)
        if alpha0 is not None:
            self.alpha_for_palette0 = int(alpha0, 16)
            self.alpha_for_palette1 = int(alpha1, 16)
            self.etc = etc
        self.crc = crc


# 0 is fully transparent, 255 is fully opaque, regardless of image bit depth / for alphas in ct3
def parse_tRNS(data, colour_type):
    if colour_type != 0 and colour_type != 2 and colour_type != 3 \
            or len(data) < 13:
        raise ValueError('tRNS chunk cannot be parsed correctly, unknown '
                         'colourtype')
    length = int(data[:8], 16)
    grey, red, blue, green, alpha0, alpha1, etc = None, None, None, None, \
                                                  None, None, None
    if colour_type == 0:
        grey = data[16:20]
    if colour_type == 2:
        red = data[16:20]
        blue = data[20:24]
        green = data[24:28]
    if colour_type == 3:
        alpha0 = data[16:18]
        alpha1 = data[18:20]
        etc = data[20:22]
    return tRNS_chunk(length, grey, red, blue, green, alpha0,
                      alpha1, etc, data[-8:])


class gAMA_chunk:
    def __init__(self, length, gamma, crc):
        self.length = int(length, 16)
        self.gamma = int(gamma, 16)
        self.crc = crc


def parse_gAMA(data):
    if len(data) != 32:
        raise ValueError('gAMA chunk cannot be parsed correctly')
    return gAMA_chunk(data[:8], data[16:18], data[-8:])


# принцип бэд текст - гуд текст тут наверное вставить надо. и вообще везде где текст
class iTXt_chunk:
    def __init__(self, length, keyword, compr_flag, compr_method, lang_tag, trns_keyword, text, crc):
        self.length = int(length, 16)
        try:
            self.keyword = codecs.decode(keyword, 'hex').decode('utf-8',
                                                                'ignore')
            self.text = codecs.decode(text, 'hex').decode('utf-8', 'ignore')
        except binascii.Error:
            self.keyword = codecs.decode(keyword[:-1], 'hex').decode('utf-8',
                                                                     'ignore')
            self.text = codecs.decode(text[:-1], 'hex').decode('utf-8',
                                                               'ignore')
        self.compression_flag = int(compr_flag, 16)
        self.compression_method = int(compr_method, 16)
        self.language_tag = lang_tag
        self.translated_keyword = codecs.decode(
            trns_keyword, 'hex').decode('utf-8', 'ignore')
        self.crc = crc


def parse_iTXt(data):
    if len(data) < 13:
        raise ValueError('iTXt chunk cannot be parsed correctly')
    first_sep = str(data).find('00', 8)
    second_sep = str(data).find('00', first_sep + 6)
    third_sep = str(data).find('00', second_sep + 2)
    return iTXt_chunk(data[:8], data[16:first_sep - 2],
                      data[first_sep:first_sep + 2],
                      data[first_sep + 2:first_sep + 4],
                      data[first_sep + 4:second_sep - 2],
                      data[second_sep:third_sep - 2],
                      data[third_sep:-8], data[-8:])


class sRGB_chunk:
    def __init__(self, length, rendering_intent, crc):
        self.length = int(length, 16)
        self.rendering_intent = int(rendering_intent, 16)
        self.crc = crc


def parse_sRGB(data):
    if len(data) < 13 or int(data[:8], 16) != 1:
        raise ValueError('sRGB chunk cannot be parsed correctly')
    return sRGB_chunk(data[:8], data[16:-8], data[-8:])


class tEXt_chunk:
    def __init__(self, length, keyword, text, crc):
        self.length = int(length, 16)
        self.keyword = codecs.decode(keyword, 'hex').decode('utf-8')
        bad_text = codecs.decode(text, 'hex').decode('utf-8', 'ignore')
        good_text = ''
        for char in bad_text:
            if char in ascii_letters or char in './\';:=-_\\ 1234567890':
                good_text += char
        self.text = good_text.replace('tEXt', ' ')
        self.crc = crc


def parse_tEXt(data):
    if len(data) < 13:
        raise ValueError('tEXt chunk cannot be parsed correctly')
    separator = re.search('00', str(data[8:]))
    if separator is None:
        raise ValueError('tEXt chunk cannot be parsed correctly')
    return tEXt_chunk(data[:8], data[16:separator.end() + 4],
                      data[separator.end() + 6:-8], data[-8:])


# ne rabotaet correctno na tst.png (crc bolshoi)
# time of last modification
class tIME_chunk:
    def __init__(self, length, year, month, day, hour, minute, second, crc):
        self.length = int(length, 16)
        self.year = int(year, 16)
        self.month = int(month, 16)
        self.day = int(day, 16)
        self.hour = int(hour, 16)
        self.minute = int(minute, 16)
        self.second = int(second, 16)
        self.crc = crc


def parse_tIME(data):
    if len(data) < 13 or int(data[:8], 16) != 7:
        raise ValueError('tIME chunk cannot be parsed correctly')
    return tIME_chunk(data[:8], data[16:20], data[20:22], data[22:24],
                      data[24:26], data[26:28], data[28:30], data[30:])


class bKGD_chunk:
    def __init__(self, length, grayscale, red, green, blue,
                 palette_index, crc):
        self.length = int(length, 16)
        if grayscale is not None:
            self.grayscale = int(grayscale, 16)
        if red is not None:
            self.red = int(red, 16)
            self.green = int(green, 16)
            self.blue = int(blue, 16)
        if palette_index is not None:
            self.palette_index = int(palette_index, 16)
        self.crc = crc


# For colour type 3 (indexed-colour), the value is the palette index of the colour to be used as background.
def parse_bKGD(data, colour_type):
    length = int(data[:8], 16)
    if colour_type != 0 and colour_type != 6 \
            and colour_type not in range(2, 5):
        raise ValueError('bKGD chunk cannot be parsed correctly')
    if (colour_type == 0 or colour_type == 4) and length != 2 or \
                    (colour_type == 2 or colour_type == 6) and length != 6 or \
                            colour_type == 3 and length != 1:
        raise ValueError('bKGD chunk cannot be parsed correctly')
    grayscale, red, green, blue, palette_index = None, None, None, None, None
    if colour_type == 0 or colour_type == 4:
        grayscale = data[16:20]
    if colour_type == 2 or colour_type == 6:
        red = data[16:20]
        green = data[20:24]
        blue = data[24:28]
    if colour_type == 3:
        palette_index = data[16:18]

    return bKGD_chunk(data[:8], grayscale, red, green, blue,
                      palette_index, data[-8:])


class cHRM_chunk:
    def __init__(self, length, white_pointX, white_pointY, redX, redY,
                 greenX, greenY, blueX, blueY, crc):
        self.length = int(length, 16)
        self.white_pointX = int(white_pointX, 16)
        self.white_pointY = int(white_pointY, 16)
        self.redX = int(redX, 16)
        self.redY = int(redY, 16)
        self.greenX = int(greenX, 16)
        self.greenY = int(greenY, 16)
        self.blueX = int(blueX, 16)
        self.blueY = int(blueY, 16)
        self.crc = crc


def parse_cHRM(data):
    if len(data) < 13:
        raise ValueError('cHRM chunk cannot be parsed correctly')
    return cHRM_chunk(data[:8], data[16:24], data[24:32], data[32:40],
                      data[40:48], data[48:56], data[56:64],
                      data[64:72], data[72:80], data[-8:])


class pHYs_chunk:
    def __init__(self, length, pxls_unitX, pxls_unitY, unit_specifier, crc):
        self.length = int(length, 16)
        self.pixels_per_unitX = int(pxls_unitX, 16)
        self.pixels_for_unitY = int(pxls_unitY, 16)
        self.unit_specifier = int(unit_specifier, 16)
        self.crc = crc


# for unit specifier:
# 0	unit is unknown
# 1	unit is the metre
def parse_pHYs(data):
    if len(data) < 13 or int(data[:8], 16) != 9:
        raise ValueError('pHYs chunk cannot be parsed correctly')
    return pHYs_chunk(data[:8], data[16:24], data[24:32],
                      data[32:34], data[-8:])


class IEND_chunk:
    def __init__(self, crc):
        self.crc = crc[-8:]


def parse_IEND(data):
    if data != b'49454e44ae426082':
        raise ValueError('IEND chunk is damaged, image cannot be parsed '
                         'correctly!')
    return IEND_chunk(data)


if __name__ == '__main__':
    main()
