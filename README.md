PNG-parser

Description:
This Parser looks at the given png-file and analyses data in each chunk of the image. It can show the statistics and the image.

Requirements:
python3, pyqt5

Contents:
parse_png.py - main file for visualisation
chunks.py - additional file with description of all png chunk for correct work of an application
png_info.py - additional file with realisation of class PngInfo for correct work of an application
tests.py - tests for application
README.md - documentation

Usage:
python3 parse_png.py
