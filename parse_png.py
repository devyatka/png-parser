import os
import sys

from PyQt5.QtCore import Qt
from PyQt5.QtGui import QIcon, QPixmap, QPainter, QColor, QPen, QImage
from PyQt5.QtWidgets import QWidget, QMainWindow, QAction, \
    QApplication, QPushButton, QInputDialog, \
    QMessageBox, QDockWidget, \
    QFileDialog, QTextEdit, QGridLayout, QLabel

import png_info


class Window(QMainWindow):
    def __init__(self):
        super().__init__()
        self.png_info = None
        self.dock = QDockWidget('  Current image: Not chosen')
        self.analysis = None
        self.initUI()

    def initUI(self):
        exit_action = QAction(QIcon('icons/exit.png'), 'Exit', self)
        exit_action.setShortcut('Esc')
        exit_action.setStatusTip('Exit')
        exit_action.setToolTip('Exit')
        exit_action.triggered.connect(self.close)

        open_action = QAction(QIcon('icons/upload.png'), 'Open file...', self)
        open_action.setShortcut('Ctrl+o')
        open_action.setToolTip('Open image')
        open_action.setStatusTip('Open image')
        open_action.triggered.connect(self.open_image)

        show_action = QAction(QIcon('icons/show.png'), 'Show image', self)
        show_action.setToolTip('Show')
        show_action.setStatusTip('Show')
        show_action.triggered.connect(self.show_image)

        analyse_action = QAction(QIcon('icons/analyse.png'), 'Analyse', self)
        analyse_action.setToolTip('Show analysis')
        analyse_action.setStatusTip('Show analysis')
        analyse_action.triggered.connect(self.show_analysis)

        help_action = QAction(QIcon('icons/help.png'), 'Get help', self)
        help_action.setToolTip('Help')
        help_action.setStatusTip('Help')
        help_action.triggered.connect(self.get_help)

        self.statusBar()
        self.addDockWidget(Qt.BottomDockWidgetArea, self.dock)
        self.setGeometry(430, 200, 1050, 600)
        self.setStyleSheet('background-color: 000055')
        self.setWindowTitle('PNG-Parser')

        menu_bar = self.menuBar()

        file_menu = menu_bar.addMenu('File')
        file_menu.addAction(open_action)
        file_menu.addAction(show_action)
        file_menu.addAction(analyse_action)
        file_menu.addAction(exit_action)

        help_menu = menu_bar.addMenu('Help')
        help_menu.addAction(help_action)

        toolbar = self.addToolBar('Commands')
        toolbar.addAction(exit_action)
        toolbar.addAction(analyse_action)
        toolbar.addAction(show_action)
        toolbar.addAction(open_action)
        toolbar.addAction(help_action)

        self.show()

    def open_image(self):
        try:
            path = QFileDialog.getOpenFileName()[0]
            filename, file_extension = os.path.splitext(path)
            short_name = filename.split('/')
            if file_extension != '.png':
                raise NameError

            with open(path, 'rb') as image:
                self.png_info = png_info.make_image_info(image)
            self.png_info.print_all_info()

            self.removeDockWidget(self.dock)
            self.dock = QDockWidget(
                '  Current image: {}'.format(short_name[len(short_name) - 1]))
            self.addDockWidget(Qt.TopDockWidgetArea, self.dock)

        except NameError:
            message_box = QMessageBox(QMessageBox.Warning, 'Error', 'Bad file')
            message_box.setText('Parser works only with PNG-files!')
            message_box.exec()
            return

        except ValueError as e:
            message_box = QMessageBox(QMessageBox.Warning, 'Error', 'Bad file')
            message_box.setText('File is damaged!\n' + str(e))
            message_box.exec()
            return

    def show_image(self):
        if self.png_info is None:
            message_box = QMessageBox(QMessageBox.Warning, 'Error', 'No file')
            message_box.setText('No image loaded')
            message_box.exec()
            return
        mode, ok = QInputDialog.getInt(self, 'Scale', 'Input percentage to '
                                                      'scale '
                                             'image to', min=1, max=100)
        if ok:
            picture = Picture(self.png_info, mode)
        else:
            return
        self.setCentralWidget(picture)

    def get_help(self):
        message_box = QMessageBox(QMessageBox.Information, 'Help', 'Help')
        message_box.setText('Png Parser \n'
                            'To load png-image press "Open file" or Ctrl+o \n'
                            'To see the analysis of the image press '
                            '"Analyse" \n'
                            'To see the image press "Show image" \n'
                            'To close the application press "Exit" or Esc')
        message_box.exec()

    def show_analysis(self):
        if self.png_info is None:
            message_box = QMessageBox(QMessageBox.Warning, 'Error', 'No file')
            message_box.setText('No image loaded')
            message_box.exec()
            return
        analysis = Analysis(self.png_info)
        self.setCentralWidget(analysis)


class Analysis(QWidget):
    def __init__(self, info):
        super().__init__()
        self.png_info = info
        self.initUI()

    def initUI(self):
        self.text = self.get_analysis()
        self.analysis = QTextEdit('Analysis of current image:\n')
        for paragraph in self.text.split('\n'):
            self.analysis.append(paragraph)
        self.analysis.setReadOnly(True)
        layout = QGridLayout()
        layout.setSpacing(5)
        layout.addWidget(self.analysis, 2, 2)
        self.setLayout(layout)
        self.show()

    def get_analysis(self):
        return self.png_info.get_all_info_str()


class Picture(QWidget):
    def __init__(self, png_info, percents):
        super().__init__()
        self.png_info = png_info
        self.percents = percents
        self.background = (QColor(255, 255, 255), QColor(192, 192, 192))
        self.bkgd_size = 30
        self.image = QImage(self.png_info.IHDR.width,
                            self.png_info.IHDR.height,
                            QImage.Format_RGB888)
        self.pixmap = QPixmap.fromImage(self.image.scaledToWidth(
            self.width() - 10))
        self.initUI()

    def initUI(self):
        self.layout = QGridLayout(self)
        self.setWindowTitle('Image')
        self.imageLabel = QLabel(self)
        self.layout.addWidget(self.imageLabel, 1, 0, 1, 4)
        self.drawImage()
        super().show()


    # def image_changed(self):
    #     pixmap = QPixmap.fromImage(self.image.scaledToWidth(
    #         self.width() - 10))
    #     self.imageLabel.setPixmap(pixmap)

    def drawImage(self):
        for i in range(self.png_info.IHDR.width * self.png_info.IHDR.height):
            x, y = i % self.png_info.IHDR.width, i // self.png_info.IHDR.width
            bkgd_r, bkgd_g, bkgd_b, a = self.background[
                (x // self.bkgd_size + y // self.bkgd_size) % 2].getRgb()
            r, g, b, alpha = self.png_info.get_rgba(x, y)
            r = round(r * alpha / 255 + bkgd_r * (1 - alpha / 255))
            g = round(g * alpha / 255 + bkgd_g * (1 - alpha / 255))
            b = round(b * alpha / 255 + bkgd_b * (1 - alpha / 255))
            pixel = QColor(r, g, b)
            self.image.setPixel(x, y, pixel.rgba())
        pixmap = QPixmap.fromImage(self.image.scaledToWidth(
            self.width() // 100 * self.percents))
        self.imageLabel.setPixmap(pixmap)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    win = Window()
    sys.exit(app.exec_())


# НЕ ЗАБЫТЬ УДАЛИТЬ

class Example(QWidget):
    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):
        self.setGeometry(300, 300, 280, 270)
        self.setWindowTitle('Pen styles')
        self.show()

    def paintEvent(self, e):
        qp = QPainter()
        qp.begin(self)
        self.drawLines(qp)
        qp.end()

    def drawLines(self, qp):
        pen = QPen(Qt.black, 2, Qt.SolidLine)

        qp.setPen(pen)
        qp.drawLine(20, 40, 250, 40)

        pen.setStyle(Qt.DashLine)
        qp.setPen(pen)
        qp.drawLine(20, 80, 250, 80)

        pen.setStyle(Qt.DashDotLine)
        qp.setPen(pen)
        qp.drawLine(20, 120, 250, 120)

        pen.setStyle(Qt.DotLine)
        qp.setPen(pen)
        qp.drawLine(20, 160, 250, 160)

        pen.setStyle(Qt.DashDotDotLine)
        qp.setPen(pen)
        qp.drawLine(20, 200, 250, 200)

        pen.setStyle(Qt.CustomDashLine)
        pen.setDashPattern([1, 4, 5, 4])
        qp.setPen(pen)
        qp.drawLine(20, 240, 250, 240)


        # The paintEvent() method is called automatically when

        # Your widget is shown for the first time.
        # After a window has been moved to reveal some part (or all) of the widget.
        # The window in which the widget lies is restored after being minimized.
        # The window in which the widget lies is resized.
        # The user switches from another desktop to the desktop on which the widget's window lies.


        # big-endian
        # budem probovat eto: b'08d74dc7410d80400004b19e2644a16945a1697891d05f35b19a22ff349d2637c67139d1973c5e12d819ed'
        # len = 86 (43 bytes)
        # bit depth = 8

        # i eto: b'78daedd6510d80400c44c1164b883a4d1585a62281fd259991b0b97b69ef14a13ec64aed1c23a44c15bb4c00081680600182052
        # 0580082050816806001081620580082052058806001081680600182052058008205081680600108162058008205205880600108168060018
        # 2052058806001081680600182052058008205081680600108162058008205205880600108168060018205205800820508168060010816205
        # 8008205205880600108168060018205205880600108168060018205205800820508168060010816205800820520588060010816806001820
        # 5205800820508168060010816205800820520588060010816806001820520588060010816806001820520580082050816806001081620580
        # 082052058806001081680600182052058001fba6aac10da394648992ae70bbab000c102102c00c102040b40b000040b102c00c102102c40b
        # 000040b40b000c102102c40b000040b40b000c102102c00c102040b40b000040b102c00c102102c40b000040b40b000c102102c00c102040
        # b40b000040b102c00c102102c40b000040b40b000c102102c40b000040b40b000c102102c00c102040b40b000040b102c00c102102c40b00
        # 0040b40b000c102102c00c102040b40b000040b102c00c102102c40b000040b40b000c102102c40b000040b40b000c102102c00c102040b4
        # 0b000040b102c00c102102c40b000040b40b000c102102c00c102040b40b000040b102c00c102102c40b000040ba0aa778c103b26887957f
        # 927acdb082e2c40b000040b40b000c102102c00c102040b40b000040b102c00c102102c40b000040b102c00c102102c40b000040b40b000c
        # 102102c00c102040b40b000040b102c00c102102c40b000040b40b000c102102c00c102040b40b000040b102c00c102102c40b000040b102
        # c00c102102c40b000040b40b000c102102c00c102040b40b000040b102c00c102102c40b000040b40b000c102102c00c102040b40b000040
        # b102c00c102102c40b000040b102c00c102102c40b000040b40b000c102102c00c102040b40b000040b102c00c102102c40b000040b40b00
        # 0c102102c00c102040b40b000040b102c00c102e8b5416e4c103facba8d10da7a8ce0c202040b40b000040b102c00c102102c40b000040b4
        # 0b000c102102c00c102040b40b000c102102c00c102040b40b000040b102c00c102102c40b000040b40b000c102102c00c102040b40b0000
        # 40b102c00c102102c40b000040b40b000c102102c00c102040b40b000c102102c00c102040b40b000040b102c00c102102c40b000040b40b
        # 000c102102c00c102040b40b000040b102c00c102102c40b000040b40b000c102102c00c102040b40b000c102102c00c102040b40b000040
        # b102c00c102102c40b000040b40b000c102102c00c102040b40b000040b102c00c102102ce0c75e3b9b1218'

        # the most significant byte comes first
        # The size of each pixel is
        #   determined by the bit depth, which is the number of bits per
        #   sample in the image data


        # Greyscale	(0) bit-depth: 1, 2, 4, 8, 16	Each pixel is a greyscale sample
        # Truecolour	2	8, 16	Each pixel is an R,G,B triple
        # Indexed-colour	3	1, 2, 4, 8	Each pixel is a palette index; a PLTE chunk shall appear.
        # Greyscale with alpha	4	8, 16	Each pixel is a greyscale sample followed by an alpha sample
        # Truecolour with alpha	6	8, 16	Each pixel is an R,G,B triple followed by an alpha sample.

        # PLTE chunk that defines the palette. Palettes may only be used when the bit depth is 1, 2, 4, or 8 bits


        # def paintEvent(self, e):
        #     qp = QPainter()
        #     qp.begin(self)
        #     self.draw_lines(qp)
        #     qp.end()
